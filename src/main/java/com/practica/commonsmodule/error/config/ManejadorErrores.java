package com.practica.commonsmodule.error.config;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.FaltaArgumento;
import com.practica.commonsmodule.error.NoEncontrado;
import com.practica.commonsmodule.error.NoTerminado;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @author pedro
 */
@ControllerAdvice
public class ManejadorErrores extends ResponseEntityExceptionHandler {

	private static final String OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR = "Ocurri� un error favor contactar al administrador.";
	private static final ConcurrentHashMap<String, Integer> CODIGOS_ESTADO = new ConcurrentHashMap<>();

	public ManejadorErrores() {
		CODIGOS_ESTADO.put(NoEncontrado.class.getSimpleName(), HttpStatus.NOT_FOUND.value());
		CODIGOS_ESTADO.put(Encontrado.class.getSimpleName(), HttpStatus.FOUND.value());
		CODIGOS_ESTADO.put(FaltaArgumento.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
		CODIGOS_ESTADO.put(NoTerminado.class.getSimpleName(), HttpStatus.FAILED_DEPENDENCY.value());
		CODIGOS_ESTADO.put("AccessDeniedException", HttpStatus.FORBIDDEN.value());
		CODIGOS_ESTADO.put("NotAuthorizedException", HttpStatus.FORBIDDEN.value());
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Error> handleAllExceptions(Exception exception) {
		ResponseEntity<Error> resultado;

		String excepcionNombre = exception.getClass().getSimpleName();
		String mensaje = exception.getMessage();
		Integer codigo = CODIGOS_ESTADO.get(excepcionNombre);

		if (codigo != null) {
			Error error = new Error(excepcionNombre, mensaje);
			resultado = new ResponseEntity<>(error, HttpStatus.valueOf(codigo));			
		} else {
			Error error = new Error(excepcionNombre, OCURRIO_UN_ERROR_FAVOR_CONTACTAR_AL_ADMINISTRADOR);
			resultado = new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return resultado;
	}

}
